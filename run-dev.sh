#!/bin/sh
source venv/bin/activate
export LOG_LEVEL=DEBUG
export FLASK_APP=manage.py
export FLASK_DEBUG=1
export AUTHLIB_INSECURE_TRANSPORT=1
flask $@