from flask import Flask
from lesite.models.db import setup_db
from lesite.auth import authbp as auth_blueprint
from lesite.index import indexbp as index_blueprint
from lesite.oauth2_server import setup_oauth2_server
from lesite.login_manager import setup_login_manager
from lesite.oauth2 import oauth2bp as oauth2_blueprint

def create_app(config=None):
    app = Flask(__name__)
    if config is not None:
        if isinstance(config, dict):
            app.config.update(config)
        elif config.endswith(".py"):
            app.config.from_pyfile(config)

    setup_config(app)
    setup_blueprints(app)
    return app

def setup_config(app):
    setup_db(app)
    setup_login_manager(app)
    setup_oauth2_server(app)

def setup_blueprints(app):
    app.register_blueprint(index_blueprint, url_prefix="")
    app.register_blueprint(auth_blueprint, url_prefix="/auth")
    app.register_blueprint(oauth2_blueprint, url_prefix="/oauth")