from uuid import uuid4
from flask import request

def str_uuid4():
    return str(uuid4())

def split_by_crlf(val:str):
    return [
        item for item in
        [item.strip() for item in val.split('\r\n')]
        if item != ''
    ]

def get_host(request):
    return "{}//{}".format(
        request.url.split("//")[0],
        request.host)

def is_safe_url(url):
    return (url is not None) and url.startswith(get_host(request))