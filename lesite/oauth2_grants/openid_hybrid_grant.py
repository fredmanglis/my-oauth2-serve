from authlib.oidc.core import grants
from lesite.models import db, AuthorizationCode
from authlib.common.security import generate_token

class OpenIDHybridGrant(grants.OpenIDHybridGrant):
    def create_authorization_code(self, client, grant_user, request):
        code = generate_token(48)
        nonce = request.data.get("nonce")
        item = AuthorizationCode(
            code=code,
            client_id=client.client_id,
            redirect_uri=request.redirect_uri,
            scope=request.scope,
            user_id=grant_user.get_user_id(),
            nonce=nonce)
        db.session.add(item)
        db.session.commit()
        return code