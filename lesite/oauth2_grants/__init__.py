from functools import wraps
from .openid_code import OpenIDCode
from .password_grant import PasswordGrant
from .refresh_token_grant import RefreshTokenGrant
from .openid_hybrid_grant import OpenIDHybridGrant
from .openid_implicit_grant import OpenIDImplicitGrant
from .authorization_code_grant import AuthorizationCodeGrant
from .grant_utilities import exists_nonce, get_jwt_config, generate_user_info

def attach_methods(cls):
    functions = [exists_nonce, get_jwt_config, generate_user_info]
    for func in functions:
        setattr(cls, func.__name__, func)

openid_flow_classes = [OpenIDCode, OpenIDImplicitGrant, OpenIDHybridGrant]
for cls in openid_flow_classes:
    attach_methods(cls)