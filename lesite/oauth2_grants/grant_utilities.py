from flask import current_app
from lesite.models import AuthorizationCode
from authlib.oidc.core import grants, UserInfo
from authlib.jose import JsonWebKey, JWK_ALGORITHMS

jwk = JsonWebKey(algorithms=JWK_ALGORITHMS)

def exists_nonce(self, nonce, request):
    exists = AuthorizationCode.query.filter_by(
        client_id=request.client_id, nonce=nonce).first()
    return bool(exists)

def get_jwt_config(self, grant):
    return {
        "key": jwk.loads(current_app.config["RSA_KEY"]), # "some-secret",# read_private_key_file(key_path),
        "alg": "RS256",
        "iss": "https://127.0.0.1:5000",
        "exp": 3600
    }

def generate_user_info(self, user, scope):
    user_info = UserInfo(sub=user.id, name=user.username)
    if "email" in scope:
        user_info["email"] = "so@me.email"
    return user_info