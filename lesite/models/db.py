from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()


def setup_db(app):
    @app.before_first_request
    def init_db():
        db.create_all()

    db.init_app(app)