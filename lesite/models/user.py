from lesite.models import db
from flask_login import UserMixin
from lesite.utilities import str_uuid4

class User(db.Model, UserMixin):
    __tablename__ = "users"

    id = db.Column(db.String(64), primary_key=True, default=str_uuid4())
    username = db.Column(db.String(40), unique=True)

    def get_user_id(self):
        return self.id

    def check_password(self, password):
        return password == "valid"

    def __str__(self):
        return "User<id={}, username={}>".format(self.id, self.username)