from lesite.models import db
from lesite.utilities import str_uuid4
from authlib.integrations.sqla_oauth2 import OAuth2ClientMixin

class Client(db.Model, OAuth2ClientMixin):
    __tablename__ = "oauth2_clients"

    id = db.Column(db.String(64), primary_key=True, default=str_uuid4())
    user_id = db.Column(
        db.String(64), db.ForeignKey("users.id", ondelete="CASCADE"))
    user = db.relationship("User")