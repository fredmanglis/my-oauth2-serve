import datetime
from lesite.index import indexbp
from lesite.models import db, Client
from flask_login import current_user
from lesite.utilities import split_by_crlf
from lesite.oauth2_server import require_oauth
from authlib.integrations.flask_oauth2 import current_token
from authlib.common.security import generate_token
from flask import request, jsonify, redirect, url_for, render_template

@indexbp.route("/", methods=["GET"])
def index():
    clients = []
    user = None
    if current_user.is_authenticated:
        clients = Client.query.filter_by(user_id=current_user.id).all()
        user = current_user
        pass
    return render_template("index.html", user=user, clients=clients)

@indexbp.route("/add_client", methods=("GET", "POST"))
def add_client():
    if not current_user.is_authenticated:
        return redirect(url_for("index.index"))
    if request.method == "GET":
        return render_template("add_client.html")
    form = request.form
    client = Client()
    client.user_id=current_user.get_user_id()
    client.client_id = generate_token(24)
    client.client_id_issued_at = datetime.datetime.now().timestamp()
    client.client_secret = ""
    if client.token_endpoint_auth_method != "none":
        client.client_secret = generate_token(48)

    client_metadata = {
        "client_name": form["client_name"],
        "client_uri": form["client_uri"],
        "grant_types": split_by_crlf(form["grant_type"]),
        "redirect_uris": split_by_crlf(form["redirect_uri"]),
        "response_types": split_by_crlf(form["response_type"]),
        "scope": form["scope"],
        "token_endpoint_auth_method": form["token_endpoint_auth_method"]
    }
    client.set_client_metadata(client_metadata)
    db.session.add(client)
    db.session.commit()
    return redirect("/")

@indexbp.route("/api/profile")
@require_oauth("openid profile")
def profile():
    user = current_token.user
    return jsonify(id=user.id, username=user.username)