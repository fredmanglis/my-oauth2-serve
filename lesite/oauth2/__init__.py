from flask import Blueprint

oauth2bp = Blueprint("oauth2", __name__)
from . import routes