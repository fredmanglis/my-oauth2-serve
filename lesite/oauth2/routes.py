from lesite.oauth2 import oauth2bp
from flask_login import current_user
from lesite.utilities import get_host
from flask import request, url_for, redirect, render_template
from lesite.oauth2_server import (
    server,
    RevocationEndpoint,
    IntrospectionEndpoint)

@oauth2bp.route("/authorize", methods=("GET", "POST"))
def authorize():
    if current_user.is_anonymous:
        next_url="{}{}".format(
            get_host(request),
            url_for(
                "oauth2.authorize",
                client_id=request.args.get("client_id"),
                scope=request.args.get("scope"),
                response_type=request.args.get("response_type"),
                nonce=request.args.get("nonce"),
                redirect_uri=request.args.get("redirect_uri")))
        return redirect(url_for("auth.login", next_url=next_url))
    if request.method == "GET":
        grant = server.validate_consent_request(end_user=current_user)
        return render_template(
            "authorization.html", grant=grant, user=current_user)
    confirmed = request.form["confirm"]
    if confirmed:
        # Granted by the resource owner
        return server.create_authorization_response(grant_user=current_user)
    # denied by resource owner
    return server.create_authorization_response(grant_user=None)

@oauth2bp.route("/token", methods=["POST"])
def token():
    return server.create_token_response()

@oauth2bp.route("/revoke", methods=["POST"])
def revoke_token():
    return server.create_endpoint_response(RevocationEndpoint.ENDPOINT_NAME)

@oauth2bp.route("/introspect", methods=["POST"])
def introspect_token():
    return server.create_endpoint_response(InstrospectionEndpoint.ENDPOINT_NAME)