from lesite.models import db, User, Token, Client
from authlib.oauth2.rfc6749.grants import ImplicitGrant
from authlib.oauth2.rfc7009 import RevocationEndpoint as _RevocationEndpoint
from authlib.oauth2.rfc7662 import (
    IntrospectionEndpoint as _IntrospectionEndpoint)
from authlib.integrations.flask_oauth2 import (
    ResourceProtector, AuthorizationServer)
from lesite.oauth2_grants import (
    OpenIDCode,
    PasswordGrant,
    RefreshTokenGrant,
    OpenIDHybridGrant,
    OpenIDImplicitGrant,
    AuthorizationCodeGrant)
from authlib.integrations.sqla_oauth2 import(
    create_save_token_func,
    create_query_client_func,
    create_bearer_token_validator,)

# Revocation Endpoint
class RevocationEndpoint(_RevocationEndpoint):
    def query_token(self, token, token_type_hint, client):
        q = Token.query.filter_by(client_id=client.client_id)
        if token_type_hint == "access_token":
            return q.filter_by(access_token=token).first()
        elif token_type_hint == "refresh_token":
            return q.filter_by(refresh_token=token).first()
        # without token_type_hint
        item = q.filter_by(access_token=token).first()
        if item:
            return item
        return q.filter_by(refresh_token=token).first()

    def revoke_token(self, token):
        token.revoked = True
        db.session.add(token)
        db.session.commit()

# Introspection Endpoint
class IntrospectionEndpoint(_IntrospectionEndpoint):
    def query_token(self, token, token_type_hint, client):
        if token_type_hint == "access_token":
            tok = Token.query.filter_by(access_token=token).first()
        elif token_type_hint == "refresh_token":
            tok = Token.query.filter_by(refresh_token=token).first()
        else: # no hint
            tok = Token.query.filter_by(access_token=token).first()
            if not tok:
                tok = Token.query.filter_by(refresh_token=token).first()
        if tok:
            if tok.client_id == client.client_id:
                return tok
            if has_introspection_permission(client):
                return tok

    def introspection_token(self, token):
        return {
            "active": True,
            "client_id": token.client_id,
            "token_type": token.token_type,
            "username": get_token_username(token),
            "scope": token.get_scope(),
            "sub": get_token_user_sub(token),
            "aud": token.client_id,
            "iss": "https://server.example.com/",
            "exp": token.expires_at,
            "iat": token.issued_at
        }

# Setup the server
server = AuthorizationServer()
require_oauth = ResourceProtector()

def setup_oauth2_server(app):
    # register grants
    server.register_grant(ImplicitGrant)
    server.register_grant(PasswordGrant)
    server.register_grant(RefreshTokenGrant)
    server.register_grant(OpenIDHybridGrant)
    server.register_grant(OpenIDImplicitGrant)
    server.register_grant(
        AuthorizationCodeGrant, [OpenIDCode(require_nonce=True)])

    # register endpoint
    server.register_endpoint(RevocationEndpoint)
    server.register_endpoint(IntrospectionEndpoint)

    # protect resources
    BearerTokenValidator = create_bearer_token_validator(db.session, Token)
    require_oauth.register_token_validator(BearerTokenValidator())

    # init server
    query_client = create_query_client_func(db.session, Client)
    save_token = create_save_token_func(db.session, Token)
    server.init_app(app, query_client=query_client, save_token=save_token)