from lesite.auth import authbp
from lesite.models import db, User
from lesite.utilities import is_safe_url
from flask_login import login_user, logout_user
from flask import abort, flash, request, redirect, url_for, render_template

@authbp.route("/login", methods=("GET", "POST"))
def login():
    next_url = request.args.get("next_url")
    if not is_safe_url(next_url):
        next_url = url_for("index.index")

    if request.method == "POST":
        form = request.form
        username = form["username"]
        password = form["password"]
        user = User.query.filter_by(username=username).first()
        if user and user.check_password(password):
            login_user(user)
            flash("Existing user logged in.")
        else:
            user = User()
            user.username = username
            db.session.add(user)
            db.session.commit()
            login_user(user)
            flash("New user created and logged in.")
            pass
        return redirect(next_url or url_for("index.index"))
    return render_template("login.html", next_url=next_url)

@authbp.route("/logout", methods=["GET"])
def logout():
    logout_user()
    return redirect(url_for("index.index"))